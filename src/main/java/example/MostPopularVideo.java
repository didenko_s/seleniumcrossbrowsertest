package example;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Larmo on 11.02.2017.
 */

public class MostPopularVideo{

    private WebDriver driver;
    private String resultChannel = "";
    private String resultVideoURL = "";
    private String resultVideoNameInList = "";
    private String resultVideoName = "";
    private boolean resultSubscribeButton = false;


    public MostPopularVideo(WebDriver driver){
        this.driver = driver;
    }

    public void runScript() throws InterruptedException {

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //search for video
        driver.get("https://www.youtube.com/");
        driver.findElement(By.id("masthead-search-term")).sendKeys("Levi9 IT services");
        driver.findElement(By.id("search-btn")).click();

        //use filters
        WebDriverWait wait = new WebDriverWait(driver, 5, 500);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@class='filter-button-container']/button")));
        driver.findElement(By.xpath(".//*[@class='filter-button-container']/button")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='filter-dropdown']/div[5]/ul/li[3]/a/span")));
        driver.findElement(By.xpath(".//*[@id='filter-dropdown']/div[5]/ul/li[3]/a/span")).click();

        //wait until page reloads & filters disappear
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@id='filter-dropdown']/div[5]/ul/li[3]/a/span")));

        //loop to find video among results
        for(int i=1; i<20; i++){

            WebElement element = driver.findElement(By.xpath(".//*[@id='results']/ol/li[2]/ol/li["+i+"]/div"));
            //if it's a video - click, else - check next element
            if (element.getAttribute("class").contains("yt-lockup-video")){
                resultVideoNameInList = driver.findElement(By.xpath(".//*[@id='results']/ol/li[2]/ol/li["+i+"]/div/div/div[2]/h3")).getText();
                driver.findElement(By.xpath(".//*[@id='results']/ol/li[2]/ol/li["+i+"]")).click();
                break;}
            else i++;
        }

        resultChannel = driver.findElement(By.className("yt-user-info")).getText();
        resultVideoURL = driver.getCurrentUrl();
        resultVideoName = driver.findElement(By.id("eow-title")).getAttribute("title");
        resultSubscribeButton = !driver.findElements(By.xpath(".//*[@id='watch7-subscription-container']/span/button[1]")).isEmpty();

    }

    public String getResultChannel(){
        return resultChannel;
    }

    public String getResultVideoURL(){
        return resultVideoURL;
    }

    public String getResultVideoNameInList(){
        return resultVideoNameInList;
    }

    public String getResultVideoName(){
        return resultVideoName;
    }

    public boolean getResultSubscribeButtonExists(){
        return resultSubscribeButton;
    }

    public void quitDriver(){
        driver.quit();
    }
}
