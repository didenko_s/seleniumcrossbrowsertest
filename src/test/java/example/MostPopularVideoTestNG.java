package example;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Created by Larmo on 11.02.2017.
 */
public class MostPopularVideoTestNG {

    WebDriver driver;
    private MostPopularVideo mpv;

    //get browser and path from TestNG.xml
    @Parameters({"browser", "path"})

    //choose driver
    @BeforeClass
    public void beforeTest(String browser, String path) throws InterruptedException {

        if (browser.equalsIgnoreCase("chrome"))
        {
            System.setProperty("webdriver.chrome.driver", path);
            driver = new ChromeDriver();
        }
        else if (browser.equalsIgnoreCase("firefox"))
        {
            System.setProperty("webdriver.gecko.driver", path);
            driver = new FirefoxDriver();
        }
        else if (browser.equalsIgnoreCase("ie"))
        {
            System.setProperty("webdriver.ie.driver", path);
            driver = new InternetExplorerDriver();
        }
        else
        {
            throw new IllegalArgumentException("The Browser Type is Undefined");
        }
    }

    @Test
    public void testRunScript() throws Exception {
        mpv = new MostPopularVideo(driver);
        mpv.runScript();
    }

    @Test(dependsOnMethods={"testRunScript"})
    public void testGetResultChannel() throws Exception {
        assertEquals(mpv.getResultChannel(),"Levi9");
    }

    @Test(dependsOnMethods={"testRunScript"})
    public void testGetResultVideoName() throws Exception {
        assertEquals(mpv.getResultVideoName(), mpv.getResultVideoNameInList());
    }

    @Test(dependsOnMethods={"testRunScript"})
    public void testGetResultSubscribeButtonExists() throws Exception {
        assertTrue(mpv.getResultSubscribeButtonExists());
    }

    //close driver after all tests are finished
    @AfterClass
    public void quitDriver() {
        mpv.quitDriver();
    }
}